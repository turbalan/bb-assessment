// Footer expanded links
let expanders = document.querySelectorAll('.expander__toggle');
expanders = Array.prototype.slice.call(expanders).forEach(element => {
    element.addEventListener('click', event => {
        if (event.target.classList.contains('is-visible')) {
            event.target.classList.remove('is-visible')
        } else {
            event.target.classList.add('is-visible');
        }
    })
});

// Toggle menu
let toggleMenuItem = document.querySelector('.menu-toggle');
let menu = document.querySelector('.header-bottom');
function toggleMenu() {
    if (toggleMenuItem.classList.contains('is-expanded')) {
        toggleMenuItem.classList.remove('is-expanded');
        menu.classList.remove('is-visible');
    } else {
        toggleMenuItem.classList.add('is-expanded');
        menu.classList.add('is-visible');
    }
}
toggleMenuItem.addEventListener('click', toggleMenu, false);

// Slideshow
var slides = document.querySelectorAll('.slideshow .slide');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide,5000);

function nextSlide() {
    if (window.innerWidth > 1024) {
        slides[currentSlide].className = 'slideshow__slide slide';
        currentSlide = (currentSlide+1)%slides.length;
        slides[currentSlide].className = 'slideshow__slide slide is-visible';
    }
}